const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
};

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}

	})
};

module.exports.updateTask = (taskId) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.status = "complete";
			return result.save().then((updatedTask, saveErr)=>{
				if(saveErr){
					console.log(saveErr);
					return false;
				}else{
					return result;
				}
			})
		}

	})
}

