const express = require("express")

const router = express.Router();

const taskController = require("../controllers/taskController");
 
 module.exports = router;
// Route to get all the tasks
// http://localhost:3001/tasks
router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/:id/complete", (req, res) =>{
	taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));
});