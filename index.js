// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup 
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://nathanieljaen12:admin123@zuitt-bootcamp.vs10cer.mongodb.net/s36?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`))

// Add task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Now listening to port ${port}`));